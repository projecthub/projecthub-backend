rm -rf projecthub-frontend
git clone https://gitlab.lrz.de/projecthub/projecthub-frontend.git --single-branch
cd projecthub-frontend
npm i -f
npm run build
#docker run --name frontend -v $(pwd)/projecthub-frontend/build:/usr/share/nginx/html:ro -p 8080:80 -d nginx
